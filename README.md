[![Android 5.0+](https://img.shields.io/badge/mobile-Android%205.0%2B-%2378C257.svg "Android 5.0+")][1]
[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][2]

# SensorWare --- Mobile sensor data collector for Android™

A mobile sensor data collector for Android™. A simple app where sensors are chosen
and configured with active time rates and thresholds each. Just a functional
demonstration that may be expanded adding sensors, storage options and better
interaction (`TODO`).

## Repository

https://gitlab.com/nunoachenriques/sensorware

## Development

### Android Studio (recommended)

 1. Fork or download project source code.
 2. Use Android Studio to open the project.
 3. Use emulators or connected devices and the usual actions.

### Command-line

 1. Fork or download project source code.
 2. Use a device connected with ADB.
 3. Test
 
    ```shell
    ./gradlew :app:connectedAndroidTest
    ```
    
 4. Install and debug
 
    ```shell
    ./gradlew installDebugRelease
    ```

### TODO

 1. Storage (refactor) + UI improvement.

 2. Sensors (add more and meta, software).

    2.1. Edit `res/xml/pref_sensors.xml` and follow the pattern there, it's documented.

    2.2. Edit `res/layout/sensor_ware.xml`

    2.3. Edit `SettingsActivity`
    
 3. Tests, tests, tests... automated, continuous (CI)...

## License

Copyright 2018 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Trademarks

Android is a trademark of Google LLC.

[1]: https://developer.android.com/about/versions/lollipop
[2]: http://www.apache.org/licenses/LICENSE-2.0.html
