/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware;

/**
 * Manages the sensors data from memory cache to persistent storage.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @param <R> Returned data type from {@link #read(Object)}.
 * @param <S> Sensor of interest.
 * @param <T> Sensors collected data type from sensor to write.
 */
public interface SensorsStorage<R, S, T> {

    /**
     * Writes sensor collected data to a persistent storage.
     *
     * @param sensor Sensor of interest.
     * @param data SensorsCollected items from sensor.
     * @return True on success, false otherwise.
     */
    boolean write(S sensor, T data);

    /**
     * Reads all previously persisted data from sensor.
     *
     * @param sensor Sensor of interest.
     * @return Read data to retrieve.
     */
    R read(S sensor);
}
