/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware.utility;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.List;

/**
 * Services utilities and management.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class Services {

    private static final String TAG = "Services";

    private Services() {}

    /**
     * Checks if a specified class is a running service.
     *
     * @param am Activity manager.
     * @param serviceClass The class which implements the service.
     * @return True if the service is running, false otherwise.
     */
    public static boolean isServiceRunning(ActivityManager am, Class<?> serviceClass) {
        if (am != null) {
            List<ActivityManager.RunningServiceInfo> services = am.getRunningServices(Integer.MAX_VALUE);
            for (ActivityManager.RunningServiceInfo service : services) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    Log.i(TAG, "Service RUNNING [" + serviceClass.getName() + "]");
                    return true;
                }
            }
        } else {
            Log.e(TAG, "Service FAILED! ActivityManager IS NULL!");
            return false;
        }
        Log.i(TAG, "Service NOT RUNNING [" + serviceClass.getName() + "]");
        return false;
    }

    /**
     * Starts a persistent service.
     *
     * @param c Context of the service, i.e., app context.
     * @param i Intent data of the service to start.
     */
    public static void startPersistentService(Context c, Intent i) {
        try {
            ComponentName cn = (i != null ? i.getComponent() : null);
            if (cn == null) {
                throw new NullPointerException("i == null | i.getComponent() == null");
            }
            // Android Oreo (>= 8.0): (new) foreground is mandatory!
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                cn = c.startForegroundService(i);
            } else {
                cn = c.startService(i);
            }
            if (cn != null) {
                Log.i(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] STARTED!");
            } else {
                throw new NullPointerException("cn == null");
            }
        } catch (SecurityException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: permissions or service file not found?", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: application not allowing in this state?", e);
        } catch (NullPointerException e) {
            Log.e(TAG, "startPersistentService FAILED! Hint: intent or component name null?", e);
        }
    }

    /**
     * Stops a persistent service.
     *
     * @param c Context of the service, i.e., app context.
     * @param i Intent data of the service to start.
     */
    public static void stopPersistentService(Context c, Intent i) {
        try {
            ComponentName cn = (i != null ? i.getComponent() : null);
            if (cn == null) {
                throw new NullPointerException("i == null | i.getComponent() == null");
            }
            if (c.stopService(i)) {
                Log.i(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] STOPPED!");
            } else {
                Log.e(TAG, cn.getClassName() + " [" + System.identityHashCode(cn) + "] FAILED to STOP!");
            }
        } catch (SecurityException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: permissions or service file not found?", e);
        } catch (IllegalStateException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: application not allowing in this state?", e);
        } catch (NullPointerException e) {
            Log.e(TAG, "stopPersistentService FAILED! Hint: intent or component name null?", e);
        }
    }
}
