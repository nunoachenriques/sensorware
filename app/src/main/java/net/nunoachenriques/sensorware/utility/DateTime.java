/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware.utility;

import android.os.SystemClock;
import android.util.Log;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * A date and time with timezone utility class. Uses ISO 8601 as default format.
 * The <a href="" title="java.time.OffsetDateTime">OffsetDateTime</a> class is
 * available since Java 1.8 and for Android since API 26. Hence this utility
 * class for Android API 23 to 25 (should be refactored if minimum API >= 26).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SimpleDateFormat
 */
@SuppressWarnings("WeakerAccess")
public final class DateTime {

    private static final String TAG = "DateTime";

    public static final long HOUR_S = 3600;
    public static final long HOUR_MS = HOUR_S * 1000;
    public static final long DAY_S = 86400;
    public static final long DAY_MS = DAY_S * 1000;
    public static final String DATE_SEPARATOR = "-";
    /** E.g., 2021-11-07 */
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    /** @see #DEFAULT_DATE_PATTERN */
    public static final String MILLISECONDS_DATE_PATTERN = DEFAULT_DATE_PATTERN;
    /** E.g., 2021-11-07T11:00:00+00:00 */
    public static final String DEFAULT_TIMESTAMP_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    /** E.g., 2021-11-07T11:00:00.000+00:00 */
    public static final String MILLISECONDS_TIMESTAMP_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
    /** E.g., +08:00 */
    public static final String DEFAULT_TIME_ZONE_PATTERN = "ZZZZZ";

    private DateTime() {}

    /**
     * Converts a duration (milliseconds) into hours [0, n] and minutes [0, 59].
     *
     * @param millis Duration in milliseconds.
     * @return The resulting [0] = hours and [1] = minutes.
     */
    public static long[] durationMillisToHourMinute(long millis) {
        return new long[]{millis / HOUR_MS, (millis % HOUR_MS) / 60000};
    }

    /**
     * Converts a local (microseconds relative to boot time) timestamp
     * (e.g., {@link android.net.wifi.WifiInfo} {@code timestamp})
     *  to SQL Timestamp.
     *
     * @return Java SQL Timestamp (milliseconds since UNIX Epoch UTC).
     * @see #bootNanosToTimestamp(long)
     */
    public static Timestamp bootMicrosToTimestamp(long bootMicros) {
        return new Timestamp(bootNanosToEpochMillis(TimeUnit.MICROSECONDS.toNanos(bootMicros)));
    }

    /**
     * Converts a local (nanoseconds relative to boot time) timestamp
     * (e.g., {@link android.hardware.SensorEvent} {@code timestamp})
     *  to SQL Timestamp. Calls {@link #bootNanosToEpochMillis(long)}.
     *
     * @return Java SQL Timestamp (milliseconds since UNIX Epoch UTC).
     */
    public static Timestamp bootNanosToTimestamp(long bootNanos) {
        return new Timestamp(bootNanosToEpochMillis(bootNanos));
    }

    /**
     * Converts an elapsed timestamp (nanoseconds relative to boot time)
     * (e.g., {@link android.hardware.SensorEvent} {@code timestamp})
     *  to Java milliseconds since UNIX Epoch UTC. Conversion formula:
     * <pre>{@code
     * ...
     *     System.currentTimeMillis()
     *      - TimeUnit.NANOSECONDS.toMillis(SystemClock.elapsedRealtimeNanos() - bootNanos)
     * ...}</pre>
     *
     * @param bootNanos Nanoseconds relative to boot (running VM) time.
     * @return Java milliseconds since UNIX Epoch UTC.
     * @see SystemClock#elapsedRealtimeNanos()
     * @see System#currentTimeMillis()
     */
    public static long bootNanosToEpochMillis(long bootNanos) {
        return System.currentTimeMillis() - TimeUnit.NANOSECONDS.toMillis(SystemClock.elapsedRealtimeNanos() - bootNanos);
    }

    /**
     * Converts the given milliseconds since UNIX Epoch timestamp in an ISO 8601
     * formatted text. Defined pattern {@code "yyyy-MM-dd"}
     * (e.g., 2017-07-12). Defined locale is {@link Locale#ROOT}.
     *
     * @param millis Milliseconds since UNIX Epoch UTC.
     * @return Date in an ISO 8601 formatted text, {@code null} on failure.
     * @see SimpleDateFormat
     * @see #MILLISECONDS_DATE_PATTERN
     */
    public static String epochMillisToDateISO8601(long millis) {
        try {
            return new SimpleDateFormat(MILLISECONDS_DATE_PATTERN, Locale.ROOT).format(millis);
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
            return null;
        }
    }

    /**
     * Converts the given milliseconds since UNIX Epoch timestamp in an ISO 8601
     * formatted text. Defined pattern {@code "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"}
     * (e.g., 2017-07-12T19:38:01.123+01:00). Defined locale is {@link Locale#ROOT}.
     *
     * @param millis Milliseconds since UNIX Epoch UTC.
     * @return Timestamp (date, time, zone) in an ISO 8601 formatted text, {@code null} on failure.
     * @see SimpleDateFormat
     * @see #MILLISECONDS_TIMESTAMP_PATTERN
     */
    public static String epochMillisToTimestampISO8601(long millis) {
        try {
            return new SimpleDateFormat(MILLISECONDS_TIMESTAMP_PATTERN, Locale.ROOT).format(millis);
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
            return null;
        }
    }

    /**
     * Converts the given local timestamp and time zone in milliseconds since
     * UNIX Epoch UTC. Time zone is an ISO 8601 formatted string as from
     * {@link #getTimeZoneISO8601()} (e.g., +08:00).
     *
     * @param ts Given timestamp (local time) to format.
     * @param tzISO8601 Given time zone to account for timestamp formatting (ts + tz).
     * @return A timestamp summed with time zone (date, time) in milliseconds.
     * @see TimeZone#getTimeZone(String)
     */
    @SuppressWarnings("unused")
    public static long localToEpoch(Timestamp ts, String tzISO8601) {
        return ts.getTime() + TimeZone.getTimeZone("GMT" + tzISO8601).getOffset(ts.getTime());
    }

    /**
     * Formats the given timestamp in milliseconds in a text with the given
     * pattern.
     *
     * @param pattern A date and time pattern (e.g., ISO 8601 custom timestamp {@code "yyyy-MM-dd HH:mm"}).
     * @param ts Given timestamp to format.
     * @return A timestamp (date, time) in a formatted text string, {@code null} if exception on pattern.
     * @see SimpleDateFormat
     */
    public static CharSequence format(String pattern, long ts) {
        try {
            return new SimpleDateFormat(pattern, Locale.ROOT).format(ts);
        } catch (Exception e) {
            Log.e(TAG, "Hint: check argument pattern!", e);
            return null;
        }
    }

    /**
     * Gets the current (Gregorian calendar now) date in an ISO 8601
     * formatted text. Defined pattern {@code "yyyy-MM-dd"}
     * (e.g., 2017-07-12). Defined locale is {@link Locale#ROOT}.
     *
     * @return Date in an ISO 8601 formatted text, {@code null} if exception on pattern or locale.
     * @see #getTimestamp(String, Locale)
     * @see #DEFAULT_DATE_PATTERN
     */
    public static String getDateISO8601() {
        return getTimestamp(DEFAULT_DATE_PATTERN, Locale.ROOT);
    }

    /**
     * Gets the current (Gregorian calendar now) timestamp in an ISO 8601
     * formatted text. Defined pattern {@code "yyyy-MM-dd'T'HH:mm:ssZZZZZ"}
     * (e.g., 2017-07-12T19:38:01+01:00). Defined locale is {@link Locale#ROOT}.
     *
     * @return Timestamp (date, time, zone) in an ISO 8601 formatted text, {@code null} if exception on pattern or locale.
     * @see #DEFAULT_TIMESTAMP_PATTERN
     */
    public static String getTimestampISO8601() {
        return getTimestamp(DEFAULT_TIMESTAMP_PATTERN, Locale.ROOT);
    }

    /**
     * Gets the current (Gregorian calendar now) timestamp in a formatted text.
     * The time zone is UTC.
     *
     * @param pattern A date and time pattern (e.g., ISO 8601 timestamp with time zone {@code "yyyy-MM-dd'T'HH:mm:ssZZZZZ"}).
     * @param locale A specific geographical, political, or cultural region locale.
     * @return A timestamp (date, time, zone) in a formatted text string, {@code null} if exception on pattern or locale.
     * @see SimpleDateFormat
     */
    public static String getTimestamp(String pattern, Locale locale) {
        try {
            return new SimpleDateFormat(pattern, locale).format(GregorianCalendar.getInstance().getTime());
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
            return null;
        }
    }

    /**
     * Gets the current (Gregorian calendar) time zone in an ISO 8601
     * formatted text. Defined pattern {@code "ZZZZZ"} (e.g., +08:00).
     * Defined locale is {@link Locale#ROOT}.
     *
     * @return Time zone in an ISO 8601 formatted text, {@code null} if exception on pattern or locale.
     * @see #getTimeZone(String, Locale)
     * @see #DEFAULT_TIME_ZONE_PATTERN
     * @see Locale#ROOT
     */
    public static String getTimeZoneISO8601() {
        return getTimeZone(DEFAULT_TIME_ZONE_PATTERN, Locale.ROOT);
    }

    /**
     * Gets the current (Gregorian calendar) time zone in a formatted text.
     *
     * @param pattern A time zone pattern like {@code "ZZZZZ"} (e.g., +08:00).
     * @param locale A specific geographical, political, or cultural region locale.
     * @return A time zone in a formatted text, {@code null} if exception on pattern or locale.
     * @see SimpleDateFormat
     */
    public static String getTimeZone(String pattern, Locale locale) {
        try {
            return new SimpleDateFormat(pattern, locale).format(GregorianCalendar.getInstance(TimeZone.getDefault()).getTime());
        } catch (Exception e) {
            Log.e(TAG, "Hint: check arguments pattern and locale!", e);
            return null;
        }
    }
}
