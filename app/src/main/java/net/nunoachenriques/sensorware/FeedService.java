/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import net.nunoachenriques.sensorware.utility.Notifications;

/**
 * The app feed {@link Service} which manages all about it: configuration,
 * starting and stopping the feed processing. Encompass all sensors: regular and
 * trigger one-shot.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see SensorWareActivity
 * @see Sensors
 */
public final class FeedService
        extends Service
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "FeedService";

    private static final int NOTIFICATION_ID = 0xFEED1;
    private static final int NOTIFICATION_RC = 0xFEED5;

    private SharedPreferences sharedPreferences;
    private Sensors sensors;

    @Override
    public void onCreate() {
        sensors = Sensors.getInstance(getApplicationContext());
        sensors.setAllSensors();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public int onStartCommand(Intent i, int flags, int sid) {
        // NOTICE: KEEP THIS 2-STEP CODE AS IS! Started service notification required to keep running in background
        startForeground(NOTIFICATION_ID, Notifications.getDefault(getApplicationContext(), true, false).build()); // To avoid ANR!
        setNotification();
        // /NOTICE
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.w(TAG, "SERVICE INTERRUPTING...");
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        sensors.unsetAllSensors();
        stopForeground(true);
        Log.w(TAG, "SERVICE INTERRUPTED!");
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        // SENSOR ID
        String[] s = key.split("\\.", 2);
        // Only of interest if an Android hardware sensor with an id number!
        if (s.length > 1 && !s[0].equals("")) {
            try {
                if (sensors.isOfInterest(Integer.valueOf(s[0]))) {
                    sensors.unsetAllSensors();
                    sensors.setAllSensors();
                }
            } catch (NumberFormatException e) {
                // not a sensor id
            }
        // CACHE
        } else if (key.equals("in_memory_capacity")
                || key.equals("in_memory_capacity_before_storage")) {
            sensors.resetCacheAndThreshold();
        }
    }

    private void setNotification() {
        final NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (nm != null) {
            final Resources res = getResources();
            Notification n = Notifications.getDefault(this, true, false)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(res.getString(R.string.app_name))
                    .setContentText(res.getString(R.string.feed_service_notification_text))
                    .setContentIntent(PendingIntent.getActivity(this, NOTIFICATION_RC,
                            new Intent(this, SensorWareActivity.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK), // https://stackoverflow.com/a/11563909/8418165
                            PendingIntent.FLAG_UPDATE_CURRENT))
                    .build();
            nm.notify(NOTIFICATION_ID, n);
        }
    }
}
