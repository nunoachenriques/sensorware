/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware;

import java.sql.Timestamp;

/**
 * Sensors collected data holder for local persistence and expanse sync.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class SensorsCollected {

    public static final String CSV_HEADER = "moment,momentTz,sensorType,sensorAttribute,sensorValue,sensorValueExtra";

    private Timestamp moment;
    /** Time zone in {@code {+|-}HH:MM} format (e.g., "+08:00") */
    private String momentTz;
    private String sensorType;
    private String sensorAttribute;
    private Double sensorValue;
    private String sensorValueExtra;

    SensorsCollected(Timestamp m, String mtz, String st, String sa, double sv, String sve) {
        moment = m;
        momentTz = mtz;
        sensorType = st;
        sensorAttribute = sa;
        sensorValue = sv;
        sensorValueExtra = sve;
    }

    @Override
    public String toString() {
        return "SensorsCollected{" + "moment='" + moment + "'" +
                ", momentTz='" + momentTz + "'" +
                ", sensorType='" + sensorType + "'" +
                ", sensorAttribute='" + sensorAttribute + "'" +
                ", sensorValue=" + sensorValue +
                ", sensorValueExtra='" + sensorValueExtra + "'" +
                "}";
    }

    public String toCSV() {
        return moment + "," + momentTz + "," + sensorType + "," + sensorAttribute + "," + sensorValue + "," + (sensorValueExtra == null ? "" : sensorValueExtra);
    }
}
