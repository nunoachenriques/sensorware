/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import net.nunoachenriques.sensorware.utility.Services;

import java.util.List;

/**
 * SensorWare main activity.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public final class SensorWareActivity extends AppCompatActivity {

    private final static String TAG = "SensorWareActivity";

    private Intent feedServiceIntent;
    private SensorsUIScheduler sensorsUIScheduler;
    private Handler sensorsUIHandler;
    private Sensors sensors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sensor_ware);
        feedServiceIntent = new Intent(getApplicationContext(), FeedService.class);
        Services.startPersistentService(getApplicationContext(), feedServiceIntent);
        sensorsUIScheduler = new SensorsUIScheduler();
        sensorsUIHandler = new Handler();
        sensors = Sensors.getInstance(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.sensor_ware, m);
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem mi) {
        switch (mi.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.menu_quit:
                Services.stopPersistentService(getApplicationContext(), feedServiceIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(mi);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // FEED service CHECK
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        if (!Services.isServiceRunning(am, FeedService.class)) {
            Services.startPersistentService(getApplicationContext(), feedServiceIntent);
        }
        // SENSORS UI update
        // TODO refactor as RecyclerView for generic sensor unit view
        sensorsUIHandler.post(sensorsUIScheduler);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorsUIHandler.removeCallbacks(sensorsUIScheduler);
    }

    /**
     * Action after clicking on sensor view in UI.
     * Should be an action of sharing or sending data to somewhere...
     *
     * @param v View which received the click event.
     */
    public void sensorViewOnClick(View v) {
        String data = sensors.getData(Integer.valueOf((String) v.getTag()));
        Intent intent = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(Intent.EXTRA_TEXT, data);
        Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.send_chooser_title));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser); // TODO: android.os.TransactionTooLargeException if data >1024kB!
        } else {
            Toast.makeText(this, R.string.send_chooser_not_available, Toast.LENGTH_LONG).show();
        }
    }

    private class SensorsUIScheduler
            implements Runnable {
        private static final long UPDATE_PERIOD_MS = 100;
        @Override
        public void run() {
            for (Integer soi : sensors.getSensorsOfInterest()) {
                List<SensorsCollected> lc = sensors.getSensorCollected(soi);
                ProgressBar pb = findViewById(getResources().getIdentifier("progress_".concat(String.valueOf(soi)), "id", getPackageName()));
                if (pb != null) {
                    boolean enabled = sensors.isEnabled(soi);
                    pb.setMax(sensors.getCacheCapacity());
                    pb.setSecondaryProgress(enabled ? sensors.getCacheToStorageThreshold() : 0);
                    pb.setSecondaryProgressTintList(ColorStateList.valueOf(enabled ? Color.GREEN : Color.RED));
                    int collected = (lc == null ? 0 : lc.size());
                    pb.setProgress(collected);
                    pb.setProgressTintList(ColorStateList.valueOf(enabled ? Color.GREEN : Color.RED));
                    Log.d(TAG, "collected: " + collected + " | threshold: " + pb.getSecondaryProgress() + " | max: " + pb.getMax());
                }
            }
            sensorsUIHandler.postDelayed(this, UPDATE_PERIOD_MS);
        }
    }
}
