/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.sensorware;

import android.content.Context;
import android.hardware.Sensor;

import java.io.*;
import java.util.List;

/**
 * Writes and reads the sensors data to a CSV text file.
 * TODO: refactor this persistent storage solution. This is a functional demo.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
final class SensorsStorageFile
        implements SensorsStorage<String, Sensor, List<SensorsCollected>> {

    private Context ctx;

    SensorsStorageFile(Context c) {
        ctx = c;
    }

    @Override
    public boolean write(Sensor sensor, List<SensorsCollected> data) {
        boolean exists = true;
        String filename = getFileName(sensor);
        try {
            ctx.openFileInput(filename);
        } catch (FileNotFoundException e) {
            exists = false;
        }
        try (PrintWriter pw = new PrintWriter(ctx.openFileOutput(filename, Context.MODE_APPEND))) {
            if (!exists) {
                pw.println(SensorsCollected.CSV_HEADER);
            }
            for (SensorsCollected c : data) {
                pw.println(c.toCSV());
            }
            pw.flush();
            pw.close();
            return !pw.checkError();
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    @Override
    public String read(Sensor sensor) {
        try {
            FileInputStream fis = ctx.openFileInput(getFileName(sensor));
            StringBuilder sb = new StringBuilder((int)fis.getChannel().size());
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            String line = reader.readLine();
            while (line != null) {
                sb.append(line).append("\n");
                line = reader.readLine();
            }
            reader.close();
            fis.close();
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private String getFileName(Sensor sensor) {
        return sensor.getName();
    }
}
